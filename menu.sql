-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 22, 2018 at 09:04 AM
-- Server version: 5.6.31
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `training`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(3) NOT NULL,
  `parent_id` int(3) NOT NULL,
  `href` varchar(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `not_access` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `parent_id`, `href`, `name`, `not_access`) VALUES
(1, 0, 'index.php?act=show_table&amp;table_name=educators', 'СОТРУДНИКИ', 'otdelenie'),
(2, 0, 'index.php?act=show_table&amp;table_name=validation', 'АТТЕСТАЦИЯ', 'otdelenie'),
(5, 0, 'index.php?act=show_table&amp;table_name=pkpp', 'ПОВЫШЕНИЕ КВАЛИФИКАЦИИ', 'st1,n.sidorova'),
(6, 0, 'index.php?act=show_table&amp;table_name=pkst', 'СТАЖИРОВКА', 'otdelenie,st1'),
(7, 0, '', 'СПРАВОЧНИКИ', 'otdelenie'),
(8, 7, 'index.php?act=show_table&amp;table_name=education', 'ОБРАЗОВАНИЕ', ''),
(9, 7, 'index.php?act=show_table&amp;table_name=level_education', 'УРОВЕНЬ ОБРАЗОВАНИЯ', ''),
(10, 7, 'index.php?act=show_table&amp;table_name=category', 'КАТЕГОРИИ', ''),
(11, 7, 'index.php?act=show_table&amp;table_name=certification', 'ВИД АТТЕСТАЦИИ', ''),
(12, 7, 'index.php?act=show_table&amp;table_name=post', 'ДОЛЖНОСТЬ', ''),
(13, 7, 'index.php?act=show_table&amp;table_name=academic_degree', 'УЧЕНАЯ СТЕПЕНЬ', ''),
(14, 7, 'index.php?act=show_table&amp;table_name=academic_title', 'УЧЕНОЕ ЗВАНИЕ', ''),
(15, 7, 'index.php?act=show_table&amp;table_name=office', 'ОТДЕЛЕНИЕ', ''),
(16, 7, 'index.php?act=show_table&amp;table_name=pk_pck', 'ПК/ПЦК', ''),
(17, 7, 'index.php?act=show_table&amp;table_name=institution', 'УЧРЕЖДЕНИЯ', ''),
(18, 7, 'index.php?act=show_table&amp;table_name=specialty', 'СПЕЦИАЛЬНОСТЬ', ''),
(19, 7, 'index.php?act=show_table&amp;table_name=training_direction', 'НАПРАВЛЕНИЕ ПОДГОТОВКИ', ''),
(20, 7, 'index.php?act=show_table&amp;table_name=kind_training', 'ФОРМА ОБУЧЕНИЯ', ''),
(21, 7, 'index.php?act=show_table&amp;table_name=users', 'ПОЛЬЗОВАТЕЛИ', 'st1,n.sidorova'),
(22, 0, '', 'ПОИСК', ''),
(23, 22, 'index.php?act=search_form&amp;search=on_Surname', 'ПО ФАМИЛИИ', ''),
(24, 22, 'index.php?act=search_form&amp;search=on_post', 'ПО ДОЛЖНОСТИ', 'st1'),
(25, 22, 'index.php?act=search_form&amp;search=on_specialty', 'ПО СПЕЦИАЛЬНОСТИ', 'st1'),
(26, 22, 'index.php?act=search_form&amp;search=on_date_st', 'ПО ДАТЕ СТАЖИРОВОК', 'st1'),
(27, 22, 'index.php?act=search_form&amp;search=Not_usual&amp;on_field=srok_st', 'ПРОСРОЧЕННЫХ ДАННЫХ ПО СТАЖИРОВКАМ', 'st1'),
(28, 22, 'index.php?act=search_form&amp;search=on_date_pk', 'ПО ДАТЕ ПОВЫШЕНИЯ КВАЛИФИКАЦИИ', 'st1'),
(29, 22, 'index.php?act=search_form&amp;search=Not_usual&amp;on_field=srok_pk', 'ПРОСРОЧЕННЫХ ДАННЫХ ПО ПОВЫШЕНИЮ КВАЛИФИКАЦИИ', 'st1'),
(30, 22, 'index.php?act=search_form&amp;search=on_kind_training', 'ПО ФОРМЕ ОБУЧЕНИЯ', 'st1'),
(31, 22, 'index.php?act=search_form&amp;search=Not_usual&amp;on_field=on_null_doc', 'ПУСТЫХ ДОКУМЕНТОВ ПО ПОВЫШЕНИЮ КВАЛИФИКАЦИИ', 'st1'),
(32, 22, 'index.php?act=search_form&amp;search=on_validation', 'ПРОСРОЧЕННЫХ АТТЕСТАЦИЙ', ''),
(34, 22, 'index.php?act=search_form&amp;search=on_atspec', 'ПО СПЕЦИАЛЬНОСТИ АТТЕСТАЦИИ', ''),
(35, 22, 'index.php?act=search_form&amp;search=on_atoffice', 'ПО ОТДЕЛЕНИЮ АТТЕСТАЦИИ', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
