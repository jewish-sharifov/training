<?
require '../connect.php';
require '../func.php';


$styleTable  = (req('act')=='show_table' && req('wk')=='4')?'width:100%;':'border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt';

$style='<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:1;
	mso-generic-font-family:roman;
	mso-font-format:other;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:2.0cm 42.5pt 2.0cm 3.0cm;
	mso-header-margin:35.4pt;
	mso-footer-margin:35.4pt;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->
</style>';
$footer="";
$header="";
if(!empty(req('wk')))
switch(req('wk')){
	case '1':
	$footer='<table width="100%"><tr><td>Согласовано<br>Заместитель директора по УМР</td><td><br><i>И.О.Фамилия</i></td></tr></table>';
	$header='<div style="vertical-align:middle;text-align:center">Министерство образования и науки Российской Федерации<br><br>Федеральное государственное бюджетное образовательное учреждение<br>высшего образования<br>
"Магнитогорский государственный технический университет им. Г.И. Носова"<br>Многопрофильный колледж</div>
<div style="text-align:right">УТВЕРЖДАЮ<br>Директор____________<i>(И.О.Фамилия)</i><br>"__" ______ 20__г.</div>
<h4 style="vertical-align:middle;text-align:center">Отчет об организации дополнительного профессионального образования <br>педагогических работников многопрофильного колледжа за ______г.</h4>';
	break;
	case '2':
	$footer="";
	$header='
<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 style="border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:none">
 <tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes">
  <td width=485 valign=top style="width:364.0pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class=MsoNormal><span><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=485 valign=top style="width:364.0pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class=MsoNormal style="text-align:justify"><span>УТВЕРЖДАЮ<o:p></o:p></span></p>
  <p class=MsoNormal style="text-align:justify"><span>Председатель аттестационной комиссии<o:p></o:p></span></p>
  <p class=MsoNormal style="text-align:justify"><span>Министерства образования и науки Российской федерации для проведения
  аттестации педагогических работников организаций, осуществляющих
  образовательную деятельность и находящихся в ведении Министерства образования
  и науки Российской федерации и Правительства Российской Федерации<o:p></o:p></span></p>
  <p class=MsoNormal style="line-height:150%"><span>________ ______Ф.И.О.<o:p></o:p></span></p>
  <p class=MsoNormal style="line-height:150%"><span>«__» ________ 20__ г.<o:p></o:p></span></p>
  </td>
 </tr>
</table>
<p class=MsoNormal><span><o:p>&nbsp;</o:p></span></p>
<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal"><span>График<o:p></o:p></span></b></p>
<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal"><span>проведения аттестации
педагогических работников<o:p></o:p></span></b></p>
<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal"><u><span>федерального
государственного бюджетного образовательного учреждения высшего<o:p></o:p></span></u></b></p>
<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal"><u><span>образования<o:p></o:p></span></u></b></p>
<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal"><u><span>«Магнитогорский
государственный технический университет им. <span class=SpellE>Г.И. Носова</span>»<o:p></o:p></span></u></b></p>
';
	break;
	case "3":
	$footer='<table width="100%"><tr><td>Согласовано<br>Заместитель директора по УМР</td><td><br><i>И.О.Фамилия</i></td></tr></table>';
	$header='<div style="vertical-align:middle;text-align:center">Министерство образования и науки Российской Федерации<br><br>Федеральное государственное бюджетное образовательное учреждение<br>высшего образования<br>
"Магнитогорский государственный технический университет им. Г.И. Носова"<br>Многопрофильный колледж</div>
<div style="text-align:right">УТВЕРЖДАЮ<br>Директор____________<i>(И.О.Фамилия)</i><br>"__" ______ 20__г.</div>
<h4 style="vertical-align:middle;text-align:center">План<br>дополнительного профессионального образования педагогических работников Многопрофильного колледжа на ______г.</h4>';
	break;
	case '4':
	$footer="";
	$header='
<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 style="border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:none">
 <tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes">
  <td width=485 valign=top style="width:364.0pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class=MsoNormal><span><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=485 valign=top style="width:364.0pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class=MsoNormal style="text-align:justify"><span>УТВЕРЖДАЮ<o:p></o:p></span></p>
  <p class=MsoNormal style="text-align:justify"><span>Председатель аттестационной комиссии<o:p></o:p></span></p>
  <p class=MsoNormal style="text-align:justify"><span>Министерства образования и науки Российской федерации для проведения
  аттестации педагогических работников организаций, осуществляющих
  образовательную деятельность и находящихся в ведении Министерства образования
  и науки Российской федерации и Правительства Российской Федерации<o:p></o:p></span></p>
  <p class=MsoNormal style="line-height:150%"><span>________ ______Ф.И.О.<o:p></o:p></span></p>
  <p class=MsoNormal style="line-height:150%"><span>«__» ________ 20__ г.<o:p></o:p></span></p>
  </td>
 </tr>
</table>
<p class=MsoNormal><span><o:p>&nbsp;</o:p></span></p>
<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal"><span>Состав аттестационной группы<o:p></o:p></span></b></p>
<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal"><span>для проведения аттестации педагогических работников
педагогических работников<o:p></o:p></span></b></p>
<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal"><u><span>федерального
государственного бюджетного образовательного учреждения высшего<o:p></o:p></span></u></b></p>
<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal"><u><span>образования<o:p></o:p></span></u></b></p>
<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal"><u><span>«Магнитогорский
государственный технический университет им. <span class=SpellE>Г.И. Носова</span>»<o:p></o:p></span></u></b></p>
';
	break;
}

?><html><head><title>ToDoc</title><?=$style?></head><body>
<?=$header?><br><table class=MsoTableGrid border=1 cellspacing=0 cellpadding=5
 style='<?=$styleTable?>'><?

switch(req('act')){
	case 'show_table':
		$arr=table();
	break;
	case 'search_form':
		if(req('wk')!='2')
		$arr=search();
	break;
}



if(req('act')=='show_table' && req('wk')=='2'){
	print forWord();
}elseif(req('act')=='show_table' && req('wk')=='4'){
	print forWord4();
}elseif(isset($arr['data'])){
	echo "<tr>";
	for($j=1;$j<$arr['row_count'];$j++){
		echo "<th>";
		echo $arr['ru'][$j];
		echo "</th>";
	}
	echo "</tr>";
	foreach($arr['data'] as $key => $val){
		echo "<tr>";
		for($j=1;$j<$arr['row_count'];$j++){
			echo "<td>";
			echo $val[$j];
			echo "</td>";
		}
		echo "</tr>";
	}
}
elseif(req('search')=='on_validation'){
	echo '<tr><th rowspan=2 >№ п/п</th><th rowspan=2>Фамили, имя, отчество аттестуемого педагогического работника</th><th rowspan=2>Должность аттестуемого педагогического работника по штатному расписанию</th><th colspan=2>Стаж педагогической работы</th><th rowspan=2>Образование, специальность, наличие наград, званий, ученой степени, ученого звания</th></tr>
<tr><td style="text-align:center" colspan="6">Высшая квалификационная категория</td></tr>';



$zapros=trim(req('zapros'));
switch(req('period')){
	case '30':
	$where="WHERE date_validation<DATE_ADD( (DATE_ADD('".$zapros."', INTERVAL 1 MONTH)),INTERVAL -5 Year)";
	$end_date=date('d.m.Y', strtotime(req('zapros').' +1 month'));
	break;
	case '60':
	$where="WHERE date_validation<DATE_ADD( (DATE_ADD('".$zapros."', INTERVAL 2 MONTH)),INTERVAL -5 Year)";
	$end_date=date('d.m.Y', strtotime(req('zapros').' +2 month'));
	break;
	case '365':
	$where="WHERE date_validation<DATE_ADD( (DATE_ADD('".$zapros."', INTERVAL 1 YEAR)),INTERVAL -5 Year)";
	$end_date=date('d.m.Y', strtotime(req('zapros').' +1 year'));
	break;
	}
	if (req('certification')!='all'){
		$where .=" and id_certification='".req('certification')."'";
	}
	if (req('certification')!='all' and req('category')!='all'){
		$where .=" and id_certification='".req('certification')."' and id_category='".req('category')."'";
	}
	if (req('category')!='all'){
		$where .=" and id_category='".req('category')."'";
	}
$q=mysqli_query($db,"SELECT CONCAT(Surname,' ', Ename,' ',Patronomic) as fio, concat(post.title_job,'/',post2.title_job)as post FROM post inner join (post as post2 inner join (educators INNER JOIN validation ON educators.id_educator = validation.educator_id) on post2.id_post=educators.post_id2) on post.id_post=educators.post_id1 ".$where);
$i=1;
while($row=$q->fetch_assoc()){
	echo "<tr>";
	echo "<td>".$i."</td>";
	$i++;
	echo "<td>".$row['fio']."</td>";
	echo "<td>".$row['post']."</td>";
	echo "<td>".date('d.m.Y',strtotime(req('zapros')))."</td>";
	echo "<td>".$end_date."</td>";
	echo "<td></td>";
	echo "</tr>";
}
}
?></table><?=$footer?>
</body></html>
