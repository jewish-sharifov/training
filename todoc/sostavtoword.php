<?
include ("../connect.php");
include("html_to_doc.inc.php");
	$htmltodoc= new HTML_TO_DOC();
	$res=mysqli_query($db, "select id_educator,concat(surname,' ',Ename,' ',patronomic) as fio, concat(post2.title_job) as npost2,concat(post.title_job) as npost1, categoryname, category_id,levelname,qualification,educ_organization,educ_specialty,academictitle,academicname,(YEAR(CURRENT_DATE)-YEAR(`date_start_education_activ`))-(RIGHT(CURRENT_DATE,5)<RIGHT(`date_start_education_activ`,5)) as stag
 from category inner join 
	(post inner join 
		(post as post2 inner join
           	(academic_title inner join
       		    (academic_degree inner join
					(educators inner join
						(level_education inner join 
                 			(education inner join validation on education.educator_id=validation.educator_id)
                 		on level_education.id_level=education.level_id)
					 on educators.id_educator=validation.educator_id)
                 on academic_degree.id_academic=educators.academic_id)
			on academic_title.id_academictitle=educators.academictitle_id)
		on post2.id_post=educators.post_id1)
	on post.id_post=educators.post_id2)
on category.id_category=validation.category_id
where id_validation in(select max(id_validation) as c from validation where educator_id in (".$_REQUEST['comissioner1'].",".$_REQUEST['comissioner2'].",".$_REQUEST['comissioner3'].") group by educator_id) ");
while ($row=$res->fetch_assoc()){
	$r[]=$row;
}
// $r=array_unique($r);
$commissioner='';
 foreach($r as $k=>$v) {
	 $commissioner.="<tr><td>".$v['fio'].", ".$v['npost2'].", ".$v['npost1']."</td><td>".$v['categoryname']."</td><td>".$v['stag']."</td><td>".$v['levelname'].", ".$v['educ_organization'].", ".$v['educ_specialty'].", ".$v['academictitle'].", ".$v['academicname']."</td></tr>";
 }
 $style='<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:1;
	mso-generic-font-family:roman;
	mso-font-format:other;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:2.0cm 42.5pt 2.0cm 3.0cm;
	mso-header-margin:35.4pt;
	mso-footer-margin:35.4pt;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->
</style>';

	$str='<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style="border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt">
 <tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes">
  <td width=274 valign=top style="width:205.55pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class=MsoNormal><o:p>&nbsp;</o:p></p>
  </td>
  <td width=350 valign=top style="width:262.2pt;padding:0cm 5.4pt 0cm 5.4pt">
  <p class=MsoNormal style="text-align:justify">УТВЕРЖДАЮ</p>
  <p class=MsoNormal style="text-align:justify">Председатель аттестационной
  комиссии</p>
  <p class=MsoNormal style="text-align:justify">Министерства образования и
  науки Российской федерации для проведения аттестации педагогических
  работников организаций, осуществляющих образовательную деятельность и
  находящихся в ведении Министерства образования и науки Российской федерации и
  Правительства Российской Федерации</p>
  <p class=MsoNormal style="line-height:150%">__________<span style="font-size:
  10.0pt;line-height:150%">(подпись)</span> _________________Ф.И.О.</p>
  <p class=MsoNormal style="line-height:150%">«____» ___________ 20__ г.</p>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal">Состав аттестационной группы<o:p></o:p></b></p>

<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal">для проведения аттестации педагогических работников<o:p></o:p></b></p>

<p class=MsoNormal align=center style="text-align:center"><b style="mso-bidi-font-weight:
normal"><u>федерального государственного бюджетного образовательного учреждения
высшего<o:p></o:p></u></b></p>

<p class=MsoNormal align=center style="margin-bottom:12.0pt;text-align:center"><b
style="mso-bidi-font-weight:normal"><u>образования «Магнитогорский
государственный технический <br>
университет им. Г.И. Носова», Многопрофильный колледж<o:p></o:p></u></b></p>
<table  class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style="border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt">
 <tr>
 <th>Фамилия, имя, отчество,
 место работы, должность 
 специалиста 
 аттестационной группы 
 председатель/специалист 
 (указать)</th>
 <th>Наличие 
 квалификационной
 категории 
(не ниже, чем у аттестуемого)</th>
 <th>Стаж
 педагогической 
 работы</th>
 <th>Образование,
 специальность, наличие наград,
 званий, ученой степени, 
 ученого звания</th>
 </tr>
 <tr>
 <td colspan=4 style="text-align:center;">'.$_REQUEST['attested'].'</td>
 </tr><tr><td colspan="4" style="text-align:center;">Состав аттестационной группы</td></tr>
'.$commissioner.'
 </table>';
	// echo $str;
	$htmltodoc->createDoc($style.$str,"sostav",true);
?>