<?php
$request = array();
$request = $_REQUEST;
$limit = 20; //LIMIT
$ru_month = array(
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
);

function save_add()
{
    global $db, $err, $request;
    $table = req('table_name');
    if (!$table and req('search'))
    {
        if (in_array(req('search') , array(
            'on_Surname',
            'on_post',
            'on_specialty',
            'on_date_st',
            'on_date_pk',
            'on_kind_training'
        ))) $table = 'pk';
        if (in_array(req('search') , array(
            'on_validation'
        ))) $table = 'validation';
    }
    if ($table == 'pkst' or $table == 'pkpp')
    {
        $table = 'pk';
    }
    $rcolumns = mysqli_query($db, "SHOW COLUMNS FROM $table");
    $q = "INSERT INTO $table ";
    $t = 0;
    $c = '';
    $v = '';
    while ($row = $rcolumns->fetch_assoc())
    {
        $col = $row['Field'];
        $val = req($col);
        if ($col == 'upassword' && strlen($val) != 32) $val = md5($val);
        $func = "null";

        $res = mysqli_query($db, "select * from tables where name='$col'")->fetch_assoc();
        $if = $res['type'];
        if ($t == 0)
        {
            $c .= "(" . $col;
            if ($if != 'hidden') $v .= "('" . $val . "'";
            else $v .= "(null";

            $t = 1;
        }
        else
        {
            $c .= " ," . $col;
            if ($if != 'hidden') $v .= " ,'" . $val . "'";
            else $v .= " ," . $func;
        }
    }
    $q .= $c . ") values " . $v . ")";
    // $err[]=$q;
    $RES = mysqli_query($db, $q);
    if (!$RES) $err[] = 'Данные не добавлены';
    unset($request['save_add']);
}
function save_edit()
{
    global $db, $request;
    $table = req('table_name');
    if (!$table and req('search'))
    {
        if (in_array(req('search') , array(
            'on_Surname',
            'on_post',
            'on_specialty',
            'on_date_st',
            'on_date_pk',
            'on_kind_training'
        ))) $table = 'pk';
        if (in_array(req('search') , array(
            'on_validation'
        ))) $table = 'validation';
    }
    if ($table == 'pkst' or $table == 'pkpp')
    {
        $table = 'pk';
    }
    $rcolumns = mysqli_query($db, "SHOW COLUMNS FROM $table");
    $q = "UPDATE $table SET ";
    $t = 0;
    $vv = true;
    while ($row = $rcolumns->fetch_array())
    {
        if ($vv)
        {
            $idd = $row[0];
            $vv = false;
            continue;
        }
        $col = $row['Field'];
        $val = req($row['Field']);
        if ($col == 'upassword' && strlen($val) != 32) $val = md5($val);

        if ($t == 0)
        {
            $q .= "$col='$val' ";
            $t = 1;
        }
        else
        {
            $q .= ",$col='$val' ";
        }
    }
    $q .= " WHERE $idd = '" . req('id') . "'";
    // $err[]=$q;
    $RES = mysqli_query($db, $q);
    if (!$RES) $err[] = 'не изменено';
    unset($request['save_edit']);
}
function del_row($table, $row)
{
    global $db, $err, $request;
    if (!$table and req('search'))
    {
        if (in_array(req('search') , array(
            'on_Surname',
            'on_post',
            'on_specialty',
            'on_date_st',
            'on_date_pk',
            'on_kind_training'
        ))) $table = 'pk';
        if (in_array(req('search') , array(
            'on_validation'
        ))) $table = 'validation';
    }
    if ($table == 'pkpp' or $table == 'pkst') $table = 'pk';
    if ($table == 'educators')
    {
        // echo $err[]='select status from educators where id_educator='.$row;
        $q = mysqli_query($db, 'select status from educators where id_educator=' . $row)->fetch_assoc();
        if ($q['status'] == 'работает') mysqli_query($db, "UPDATE educators SET `status` = 'уволен' WHERE id_educator=" . $row);
        else mysqli_query($db, "DELETE FROM educators WHERE id_educator=" . $row);
    }
    else
    {
        $rcolumns = mysqli_query($db, "SHOW COLUMNS FROM $table")->fetch_array();
        mysqli_query($db, "DELETE FROM $table WHERE $rcolumns[0] = $row");
    }
    unset($request['del_row']);
}
function select($name, $field, $table, $selected = false)
{
    global $db;
    $resid = mysqli_query($db, "SHOW COLUMNS FROM $table")->fetch_array();
    $a = '<select name="' . $name . '" >';
    if (!$selected) $a .= '<option selected disabled>выберите</option>';
    $select = mysqli_query($db, "select $resid[0] as id, concat($field) as fiel from $table");
    while ($sel = $select->fetch_assoc())
    {
        $seld = '';
        if ($selected == $sel['id']) $seld = 'selected';
        $a .= '<option value="' . $sel['id'] . '" ' . $seld . '>' . $sel['fiel'] . '</option>';
    }
    $a .= '</select>';
    return $a;
}
function input_dataset($name, $field, $table, $val = false, $required = true)
{
    global $db;

    if ($required) $r = 'required';
    else $r = '';
    $a = $name . '<br><input type="text" name="' . $field . '" list="list' . $field . '" value = "' . $val . '" ' . $r . '><datalist id="list' . $field . '">';
    $dbresult = mysqli_query($db, "Select $field From  $table group by $field");
    if (!$dbresult) die("Ошибка доступа к таблице");
    while ($training = $dbresult->fetch_assoc())
    {
        $a .= '<option>' . $training[$field] . '</option>';
    }
    $a .= '</datalist><p>';
    return $a;
}
function req($a)
{
    global $db, $request;
    if (isset($request[$a])) $b = str_replace("`", "``", mysqli_real_escape_string($db, $request[$a]));
    else $b = false;
    return $b;
}
function show_table($arr)
{
    global $request;

    if (is_array($arr) and isset($_SESSION['is_auth']))
    {

        $th = "<tr>";
        for ($j = 1;$j < $arr['row_count'];$j++)
        {
            $_width = false;
            $_colspan = $_colspan2 = false;

            $_text = $arr['ru'][$j];

            if ($request['table_name'] == 'educators' && in_array($j, [5, 6])) $_width = 'width="80px"';
            if ($request['table_name'] == 'educators' && in_array($j, [1, 2, 3, 7, 8]))
            {

                if ($j == 1)
                {
                    $_colspan = 'colspan="3"';
                    $_text = 'Преподаватель';
                }
                else if ($j == 7)
                {
                    $_colspan2 = 'colspan="2"';
                    $_text = 'Ученая степень/звание';
                }
                else continue;
            }

            $th .= '<th ' . ($_width ? $_width : null) . ' ' . ($_colspan ? $_colspan : null) . ' ' . ($_colspan2 ? $_colspan2 : null) . ' >';
            $th .= $_text;
            $th .= '</th>';
        }
        $th .= "<th class=actionbutton colspan=2>Действия</th></tr>";
        $tr = '';

        foreach ($arr['data'] as $key => $val)
        {
            $tr .= '<tr id="' . $val[0] . '" class="rows">';
            for ($j = 1;$j < $arr['row_count'];$j++)
            {

                $_colspan = $_colspan2 = false;
                $_text = $val[$j];

                if ($request['table_name'] == 'educators' && in_array($j, [1, 2, 3, 7, 8]))
                {

                    if ($j == 1)
                    {
                        $_colspan = 'colspan="3"';
                        $_text = $val[1] . ' ' . $val[2] . ' ' . $val[3];
                    }
                    else if ($j == 7)
                    {
                        $_colspan2 = 'colspan="2"';
                        $_text = $val[7] . ' ' . $val[8];
                    }
                    else continue;
                }

                $tr .= '<td ' . ($_colspan ? $_colspan : null) . ' ' . ($_colspan2 ? $_colspan2 : null) . ' >';
                $tr .= $_text;
                $tr .= '</td>';
            }
            $tr .= '<td class="actionbutton" onclick="$(\'#okno\').show(); XHR(\'edit.php?' . http_build_query($request) . '&id=' . $val[0] . '\', fff);"><img src=/images/edit2.png></td><td class=actionbutton onclick="document.location.href=\'index.php?' . http_build_query($request) . '&del_row=' . $val[0] . '\';"><img src=/images/delete2.png></td></tr>';
        }
        return '<table name="output_table" style="text-align:center" width="100%" id="output_table" ><tbody>' . $th . $tr . '</tbody><style>#print table{border-collapse: collapse;} #print td,#print th{border:1px solid black;} #print .actionbutton{display:none;} #print #content{display:none;} #print .reference{display:none;}        </style>' . getPagination($arr['row_count']) . '</table>';
    }
    else return false;
}

function getPagination($count)
{
    global $limit;

    $html = '';
    $page = 1;
    $count += 1;

    parse_str($_SERVER['QUERY_STRING'], $output);

    if ($output['page'])
    {
        $page = $output['page'];
        unset($output['page']);
    }

    $baseurl = http_build_query($output);

    $all = table(null, true);

    $size = ceil(count($all['data']) / $limit);

    $html .= '<tr>
                        <td align="center" colspan="' . ($count) . '">
                                <div class="pagination">';

    for ($i = 1;$i <= $size;$i++) $html .= '<a ' . ($i == $page ? 'class="active"' : '') . ' href="/index.php?' . $baseurl . '&page=' . $i . '">' . $i . '</a>';

    $html .= '</div>
                        </td>
                </tr>';

    return $html;
}

function forWord()
{
    global $db, $ru_month;

    $arr = '<style>
                                 #forword td,#forword th {text-align:center}
                                 .bor{border-bottom:1px solid #2f2f2f}
                                 .bor1{border-left:1px solid #2f2f2f}
                                 .w33{width:33px}
                                 .p10{padding:10px}
                  </style>
                  <table id="forword" border=1 cellspacing=0 cellpadding=0>
                        <tr>
                                <th class="w33">№ п/п</th>
                                <th>Фамилия, имя, отчество аттестуемого педагогического работника</th>
                                <th>Должность аттестуемого педагогического работника по штатному расписанию</th>
                                <th>
                                        <table cellspacing="0" cellpadding="10">
                                                <tr><th colspan="2" class="bor">Сроки работы аттестационной группы</th></tr>
                                                <tr><th width="90px">начало</th><th class="bor1">окончание</th></tr>
                                        </table>
                                </th>
                                <th>Сроки принятия решения аттестационной комиссией</th>
                        </tr>';

    $q = mysqli_query($db, 'SELECT Surname, Ename, Patronomic, p.title_job, v.date_validation, c.categoryname FROM validation v INNER JOIN educators ed ON v.educator_id=ed.id_educator INNER JOIN category c ON c.id_category=v.category_id INNER JOIN post p ON p.id_post=ed.post_id1 ORDER BY id_category DESC');

    $res = [];

    while ($r = $q->fetch_array()) $res[$r['categoryname']][] = $r;

    $i = 1;
    foreach ($res as $key => $r)
    {
        $arr .= '<tr><th colspan="5" class="p10">' . $key . ' квалификационная категория</th></tr>';
        foreach ($r as $r1)
        {
            $arr .= "<tr>
                                                <td>{$i}</td>
                                                <td>{$r1['Surname']} {$r1['Ename']} {$r1['Patronomic']}</td>
                                                <td>{$r1['title_job']}</td>
                                                <td>
                                                        <table cellspacing='0' cellpadding='3' width='100%'>
                                                                <td>" . date('d.m.Y', strtotime($r1['date_validation'])) . "</td>
                                                                <td class='bor1'>" . date('d.m.Y', strtotime($r1['date_validation'] . ' + 1 month')) . "</td>
                                                        </table>
                                                </td>
                                                <td>" . $ru_month[date('n', strtotime($r1['date_validation'] . ' + 2 month')) ] . ' ' . date('Y', strtotime($r1['date_validation'])) . "</td>
                                   </tr>";
            $i++;
        }
    }

    $arr .= '</table>';

    return $arr;
}

function forWord4()
{
    global $db;
    $arr = '<tr>
                                <th>№ п/п</th>
                                <th>Фамилия, имя, отчество, место работы, должность специалиста аттестационной группы председатель/специалист <br/> (указать)</th>
                                <th>Наличие квалификационной категории <br/> (не ниже, чем у аттестуемого)</th>
                                <th>Стаж педагогической работы</th>
                                <th>Образование, специальность, наличие наград, званий, ученой степени, ученого звания</th>
                        </tr>';

    $q = mysqli_query($db, 'SELECT ed.Surname, ed.Ename, c.categoryname, ed.Patronomic, v.commissioner1, v.commissioner2, v.commissioner3, p.title_job FROM validation v INNER JOIN educators ed ON v.educator_id=ed.id_educator INNER JOIN category c ON c.id_category = v.category_id INNER JOIN post p ON p.id_post = ed.post_id1');

    $res = [];

    while ($r = $q->fetch_object())
    {
        $name = $r->Surname . ' ' . $r->Ename . ' ' . $r->Patronomic;
        $arr .= '<tr><th colspan="5">' . $name . ', ' . $r->title_job . ', ' . $r->categoryname . '  категория</th></tr>
                           <tr><th colspan="5">Состав аттестационной группы</th></tr>';

        $q1 = mysqli_query($db, "SELECT ed.Surname, ed.Ename, c.categoryname, a.academicname, e.educ_organization, e.educ_specialty, e.qualification, l.levelname, ed.Patronomic, p.title_job, i.title_institution FROM educators ed LEFT JOIN post p ON p.id_post = ed.post_id1 LEFT JOIN pk ON pk.educator_id = ed.id_educator LEFT JOIN institution i ON i.id_institution = pk.institution_id LEFT JOIN education e ON e.educator_id = ed.id_educator LEFT JOIN academic_degree a ON a.id_academic=ed.academic_id LEFT JOIN level_education l ON l.id_level = e.level_id LEFT JOIN validation v ON v.educator_id=ed.id_educator LEFT JOIN category c ON c.id_category = v.category_id WHERE ed.id_educator ='{$r->commissioner1}' OR ed.id_educator ='{$r->commissioner2}' OR ed.id_educator ='{$r->commissioner3}' GROUP BY ed.id_educator");

        $i = 0;

        while ($r1 = $q1->fetch_object())
        {
            $i++;
            $sps = mb_strtolower($r1->title_job, 'UTF-8') == 'директор' ? 'председатель' : 'специалист';
            $t[0] = !empty($r1->title_institution) ? $r1->title_institution . ',' : null;
            $t[1] = !empty($r1->levelname) ? $r1->levelname . ',' : null;
            $t[2] = !empty($r1->educ_organization) ? $r1->educ_organization . ',' : null;
            $t[3] = !empty($r1->educ_specialty) ? $r1->educ_specialty . ',' : null;
            $t[4] = !empty($r1->qualification) ? $r1->qualification . ',' : null;

            $arr .= "<tr>
                                                <td align=\"center\">{$i}</td>
                                                <td align=\"center\">{$r1->Surname} {$r1->Ename} {$r1->Patronomic},  {$t[0]} <br/> {$r1->title_job}, {$sps} аттестационной группы</td>
                                                <td align=\"center\">{$r1->categoryname}</td>
                                                <td align=\"center\">" . (date('Y') - date('Y', strtotime($r1->date_start_education_activ))) . "</td>
                                                <td align=\"center\">{$t[1]} {$t[2]} <br/> {$t[3]} <br/> {$t[4]} {$r1->academicname}</td>
                                   </tr>";
            if ($i == 3) $i = 0;
        }

    }

    return $arr;
}

$select_categories = '';
$q = mysqli_query($db, "select * from category");
while ($r = mysqli_fetch_object($q)) $select_categories .= '<option ' . ($r->id_category == req('category') ? 'selected="selected"' : null) . ' value="' . $r->id_category . '">' . $r->categoryname . '</option>';

$select_certification = '';
$q = mysqli_query($db, "select * from certification");
while ($r = mysqli_fetch_object($q)) $select_certification .= '<option ' . ($r->id_certification == req('view') ? 'selected="selected"' : null) . ' value="' . $r->id_certification . '">' . $r->certificationname . '</option>';

function table($page = 1, $all = false)
{
    global $db, $order, $limit, $where;
    $arr = array();
    $fields = '';
    if (!$page) $page = 1;
    $offset = ($page - 1) * $limit;
    $pkst = '';
    if (req('table_name'))
    {
        $table = req('table_name');
        if ($table == 'pkst' or $table == 'pkpp')
        {
            $table = 'pk';
            $pkst = req('table_name');
        }
        $there = $table;
    }

    if (isset($table))
    {
        $q = mysqli_query($db, "select * from " . $table);

        $arr['row_count'] = mysqli_num_fields($q);
        $col = mysqli_query($db, "SHOW COLUMNS FROM " . $table);
        $c = 0;
        while ($row = $col->fetch_array())
        {
            // if($fields=='') $fields=$row['0'];
            $ru = mysqli_query($db, "select * from tables where name='" . $row['0'] . "'");
            if (mysqli_num_rows($ru) > 0)
            {
                $tab = $ru->fetch_array();
                $arr['ru'][] = $tab['ru'];
                if ($tab['kind_tool'] == 'select')
                {
                    $table = "(" . $tab['where'] . " inner join " . $table . " on " . $tab['where'] . "." . $tab['onfield'] . "=" . $there . "." . $tab['name'] . ")";
                    if ($fields == '')
                    {
                        $fields = "concat(" . $tab['what'] . ")";
                    }
                    else
                    {
                        $fields = $fields . " ,concat(" . $tab['what'] . ")";
                    }
                }
                else
                {
                    if ($fields == '')
                    {
                        $fields = $tab['name'];
                    }
                    else
                    {
                        $fields = $fields . " ," . $tab['name'];
                    }
                }
            }

        }

        if (is_array($where))
        {
            $w = 'WHERE';
            foreach ($where as $k => $h) $w .= " {$k}='{$h}' AND ";
            $w = rtrim($w, ' AND ');
        }
        else $w = '';

        // echo $table;
        //
        // echo $fields;
        $zapros = "select " . $fields . " from " . $table;

        // echo $zapros;
        if ($there == 'educators')
        {
            $zapros = "select id_educator,Surname ,Ename ,Patronomic ,birthday ,date_start_employment ,date_start_education_activ ,concat(academicname) ,concat(academictitle), concat(post2.title_job)  ,concat(post.title_job),concat(officename) ,concat(PK_PCKname),status from pk_pck inner join (office inner join (post inner join (post as post2 inner join (academic_title inner join (academic_degree inner join educators on academic_degree.id_academic=educators.academic_id) on academic_title.id_academictitle=educators.academictitle_id) on post2.id_post=educators.post_id1) on post.id_post=educators.post_id2)on office.id_office=educators.office_id) on pk_pck.id_pk_pck=educators.pk_pck_id";
        }
        if ($there == 'validation')
        {

            $zapros = "SELECT id_validation,CONCAT( educators4.Surname,' ', educators4.Ename,' ',educators4.Patronomic) as educ, CONCAT( categoryname ) , CONCAT( certificationname ) , date_validation, num_order, CONCAT( educators3.Surname,  ' ', educators3.Ename,  ' ', educators3.Patronomic ) , CONCAT( educators2.Surname,  ' ', educators2.Ename,  ' ', educators2.Patronomic ) , CONCAT( educators.Surname,  ' ', educators.Ename,  ' ', educators.Patronomic ) FROM (educators INNER JOIN (educators AS educators2 INNER JOIN (educators AS educators3 INNER JOIN(certification INNER JOIN (category INNER JOIN (educators AS educators4 INNER JOIN validation ON educators4.id_educator = validation.educator_id) ON category.id_category = validation.category_id) ON certification.id_certification = validation.certification_id) ON educators3.id_educator = validation.commissioner1) ON educators2.id_educator = validation.commissioner2) ON educators.id_educator = validation.commissioner3) {$w}";
        }
        if ($pkst == 'pkst')
        {
            $zapros = "select id_pk,concat(Surname,' ',Ename,' ',Patronomic) as educ ,Doc_num ,Doc_date ,num_hours ,concat(title_kind) ,concat(title_institution) ,Program ,professional_module ,concat(title_specialty,' ',name_specialty) ,concat(title_direction) from (training_direction inner join (specialty inner join (institution inner join (kind_training inner join (educators inner join pk on educators.id_educator=pk.educator_id) on kind_training.id_kind_training=pk.kind_tr_id) on institution.id_institution=pk.institution_id) on specialty.id_specialty=pk.specialty_id) on training_direction.id_training_direction=pk.training_direction_id) where kind_tr_id=2";
        }
        if ($pkst == 'pkpp')
        {
            $zapros = "select id_pk,concat(Surname,' ',Ename,' ',Patronomic) as educ ,Doc_num ,Doc_date ,num_hours ,concat(title_kind) ,concat(title_institution) ,Program ,professional_module ,concat(title_specialty,' ',name_specialty) ,concat(title_direction) from (training_direction inner join (specialty inner join (institution inner join (kind_training inner join (educators inner join pk on educators.id_educator=pk.educator_id) on kind_training.id_kind_training=pk.kind_tr_id) on institution.id_institution=pk.institution_id) on specialty.id_specialty=pk.specialty_id) on training_direction.id_training_direction=pk.training_direction_id) where kind_tr_id in (1,3)";
        }
        $zapros .= $order;

        $zapros .= !$all ? " LIMIT {$offset}, {$limit}" : '';

        // echo $zapros;        // для проверки запроса выводим и копируем в phpmyadmin
        $res = mysqli_query($db, $zapros);
        $arr['data'] = array();
        while ($row = $res->fetch_array())
        {
            $arr['data'][] = $row;
        }

        return $arr;

    }
    else return false;
}
function other_table($search){
    global $request, $db;
    $zapros = req('zapros');
    $th='
        <tr>
            <th>Преподаватель</th>
            <th>Категория</th>
            <th>Вид аттестации</th>
            <th>Дата аттестации</th>
            <th>Номер приказа</th>
            <th>Член комиссии 1</th>
            <th>Член комиссии 2</th>
            <th>Член комиссии 3</th>
        </tr>
    ';

    switch($search){
        case 'on_atsurname':
            $q="SELECT id_validation,CONCAT( educators4.Surname,' ', educators4.Ename,' ',educators4.Patronomic) as educ, CONCAT( categoryname ) , CONCAT( certificationname ) , date_validation, num_order, CONCAT( educators3.Surname,  ' ', educators3.Ename,  ' ', educators3.Patronomic ) , CONCAT( educators2.Surname,  ' ', educators2.Ename,  ' ', educators2.Patronomic ) , CONCAT( educators.Surname,  ' ', educators.Ename,  ' ', educators.Patronomic ) FROM (educators INNER JOIN (educators AS educators2 INNER JOIN (educators AS educators3 INNER JOIN (certification INNER JOIN (category INNER JOIN (post INNER JOIN (educators AS educators4 INNER JOIN validation ON educators4.id_educator = validation.educator_id ) ON post.id_post = educators4.post_id1 ) ON category.id_category = validation.category_id ) ON certification.id_certification = validation.certification_id ) ON educators3.id_educator = validation.commissioner1 ) ON educators2.id_educator = validation.commissioner2 ) ON educators.id_educator = validation.commissioner3 ) WHERE  post.title_job LIKE '%{$zapros}%'";
        break;
        case 'on_Surname':
            $q="SELECT id_validation,CONCAT( educators4.Surname,' ', educators4.Ename,' ',educators4.Patronomic) as educ, CONCAT( categoryname ) , CONCAT( certificationname ) , date_validation, num_order, CONCAT( educators3.Surname,  ' ', educators3.Ename,  ' ', educators3.Patronomic ) , CONCAT( educators2.Surname,  ' ', educators2.Ename,  ' ', educators2.Patronomic ) , CONCAT( educators.Surname,  ' ', educators.Ename,  ' ', educators.Patronomic ) FROM (educators INNER JOIN (educators AS educators2 INNER JOIN (educators AS educators3 INNER JOIN (certification INNER JOIN (category INNER JOIN (post INNER JOIN (educators AS educators4 INNER JOIN validation ON educators4.id_educator = validation.educator_id ) ON post.id_post = educators4.post_id1 ) ON category.id_category = validation.category_id ) ON certification.id_certification = validation.certification_id ) ON educators3.id_educator = validation.commissioner1 ) ON educators2.id_educator = validation.commissioner2 ) ON educators.id_educator = validation.commissioner3 ) WHERE  educators4.Surname LIKE '%{$zapros}%'";
        break;
        case 'on_atspec':
            $q="SELECT id_validation,CONCAT( educators4.Surname,' ', educators4.Ename,' ',educators4.Patronomic) as educ, CONCAT( categoryname ) , CONCAT( certificationname ) , date_validation, num_order, CONCAT( educators3.Surname,  ' ', educators3.Ename,  ' ', educators3.Patronomic ) , CONCAT( educators2.Surname,  ' ', educators2.Ename,  ' ', educators2.Patronomic ) , CONCAT( educators.Surname,  ' ', educators.Ename,  ' ', educators.Patronomic ) FROM ( educators INNER JOIN (educators AS educators2 INNER JOIN (educators AS educators3 INNER JOIN (certification INNER JOIN (category INNER JOIN (specialty INNER JOIN (pk INNER JOIN (educators AS educators4 INNER JOIN validation ON educators4.id_educator = validation.educator_id) ON pk.educator_id = educators4.id_educator) ON pk.specialty_id = specialty.id_specialty) ON category.id_category = validation.category_id) ON certification.id_certification = validation.certification_id) ON educators3.id_educator = validation.commissioner1) ON educators2.id_educator = validation.commissioner2) ON educators.id_educator = validation.commissioner3) WHERE specialty.name_specialty LIKE '%{$zapros}%'";
        break;
        case 'on_atoffice':
            $q="SELECT id_validation,CONCAT( educators4.Surname,' ', educators4.Ename,' ',educators4.Patronomic) as educ, CONCAT( categoryname ) , CONCAT( certificationname ) , date_validation, num_order, CONCAT( educators3.Surname,  ' ', educators3.Ename,  ' ', educators3.Patronomic ) , CONCAT( educators2.Surname,  ' ', educators2.Ename,  ' ', educators2.Patronomic ) , CONCAT( educators.Surname,  ' ', educators.Ename,  ' ', educators.Patronomic ) FROM (educators INNER JOIN (educators AS educators2 INNER JOIN (educators AS educators3 INNER JOIN (certification INNER JOIN (category INNER JOIN (office INNER JOIN (educators AS educators4 INNER JOIN validation ON educators4.id_educator = validation.educator_id )ON office.id_office = educators4.office_id )ON category.id_category = validation.category_id )ON certification.id_certification = validation.certification_id )ON educators3.id_educator = validation.commissioner1 )ON educators2.id_educator = validation.commissioner2 )ON educators.id_educator = validation.commissioner3 ) WHERE office.officename LIKE '%{$zapros}%'";
        break;
    }

    $res = mysqli_query($db, $q);
    $tr='';
    $j=0;
    while ($row = $res->fetch_array())
    {
        $tr.='<tr>';
        for($i=1;$i<=8;$i++){
            $tr.='
                <td>'.$row[$i].'</td>
            ';
        }
        $tr.='</tr>';
        //$tr .= '<td class="actionbutton" onclick="$(\'#okno\').show(); XHR(\'edit.php?' . http_build_query($request) . '&id=' . $row[0] . '\', fff);"><img src=/images/edit2.png></td><td class=actionbutton onclick="document.location.href=\'index.php?' . http_build_query($request) . '&del_row=' . $row[0] . '\';"><img src=/images/delete2.png></td></tr>';
        $j++;

    }
    return '<table name="output_table" style="text-align:center" width="100%" id="output_table" ><tbody>' . $th . $tr . '</tbody><style>#print table{border-collapse: collapse;} #print td,#print th{border:1px solid black;} #print .actionbutton{display:none;} #print #content{display:none;} #print .reference{display:none;}        </style></table>';
}

function search()
{
    global $db, $err;
    $tab = req('on_field'); //параметр поиска

    $tabel = 'pk';
    if ($tab == 'date_validation' or req('in') == 'validation') $tabel = 'validation';
    $zapros = trim(req('zapros')); //значение поиска
    $col = mysqli_query($db, "SHOW COLUMNS FROM $tabel");
    $arr['row_count'] = mysqli_num_rows($col); //количество полей
    while ($row = $col->fetch_array())
    {
        $ru = mysqli_query($db, "select * from tables where name='" . $row['0'] . "'");
        if (mysqli_num_rows($ru) > 0)
        {
            $t = $ru->fetch_array();
            $arr['ru'][] = $t['ru'];
        }
    }
    if ($tab == 'surname')
    {
        $info = mysqli_query($db, "select * from educators where Surname='" . $zapros . "'");
        if (mysqli_num_rows($info) > 0)
        {
            $info = $info->fetch_array();
            $arr['surname'] = $info['Surname'];
        }
    }
    if (req('zapros') or $tab == "srok_pk" or $tab == "srok_st" or $tab == "on_null_doc")
    {
        switch ($tab)
        {
            case 'title_job':
                $where = "WHERE post.title_job like '%$zapros%' or post2.title_job like '%$zapros%'";
            break;
            case 'id_kind_tr':
                $where = "WHERE id_kind_training=$zapros";
            break;
            case 'date_validation':

                switch (req('period'))
                {
                    case '30':
                        $where = "WHERE date_validation<DATE_ADD( (DATE_ADD('" . $zapros . "', INTERVAL 1 MONTH)),INTERVAL -5 Year)";
                    break;
                    case '60':
                        $where = "WHERE date_validation<DATE_ADD( (DATE_ADD('" . $zapros . "', INTERVAL 2 MONTH)),INTERVAL -5 Year)";
                    break;
                    case '365':
                        $where = "WHERE date_validation<DATE_ADD( (DATE_ADD('" . $zapros . "', INTERVAL 1 YEAR)),INTERVAL -5 Year)";
                    break;
                }
                if (req('certification') != 'all')
                {
                    $where .= " and id_certification='" . req('certification') . "'";
                }
                if (req('certification') != 'all' and req('category') != 'all')
                {
                    $where .= " and id_certification='" . req('certification') . "' and id_category='" . req('category') . "'";
                }
                if (req('category') != 'all')
                {
                    $where .= " and id_category='" . req('category') . "'";
                }
            break;
            case 'date_st':
                $where = "where doc_date < Date_add('" . $zapros . "',Interval -3 Year) and
id_educator not in ( select educator_id from pk where doc_date between Date_add('" . $zapros . "',Interval -3 Year)  and '" . $zapros . "' and kind_tr_id=2) and id_kind_training=2";
            break;
            case 'date_pk':
                $where = "where doc_date < Date_add('" . $zapros . "',Interval -5 Year) and
id_educator not in ( select educator_id from pk where doc_date between Date_add('" . $zapros . "',Interval -5 Year)  and '" . $zapros . "' and kind_tr_id=1) and id_kind_training=1";
            break;
            case 'srok_pk':
                $where = "where doc_date < Date_add(Now(),Interval -5 Year) and
id_educator not in ( select educator_id from pk where doc_date between Date_add(Now(),Interval -5 Year)  and Now() and kind_tr_id=1) and id_kind_training=1";
            break;
            case 'srok_st':
                $where = "where doc_date < Date_add(Now(),Interval -3 Year) and
id_educator not in ( select educator_id from pk where doc_date between Date_add(Now(),Interval -3 Year)  and Now() and kind_tr_id=2) and id_kind_training=2";
            break;
            case 'on_null_doc':
                $where = "where  Doc_num=''";
            break;
            default:
                if(in_array(5, $_SESSION['not_access'])) $tab='educators4.'.$tab;
                $where = "WHERE " . $tab . " like '%$zapros%'";
        }

        switch ($tabel)
        {
            case 'pk':
              if(in_array(5, $_SESSION['not_access']))
                $zapros = "SELECT id_validation, CONCAT( educators4.Surname,' ', educators4.Ename,' ',educators4.Patronomic) as educ, CONCAT( categoryname ) , CONCAT( certificationname ) , date_validation, num_order, CONCAT( educators3.Surname,  ' ', educators3.Ename,  ' ', educators3.Patronomic ) , CONCAT( educators2.Surname,  ' ', educators2.Ename,  ' ', educators2.Patronomic ) , CONCAT( educators.Surname,  ' ', educators.Ename,  ' ', educators.Patronomic ) FROM (educators INNER JOIN (educators AS educators2 INNER JOIN (educators AS educators3 INNER JOIN(certification INNER JOIN (category INNER JOIN (educators AS educators4 INNER JOIN validation ON educators4.id_educator = validation.educator_id) ON category.id_category = validation.category_id) ON certification.id_certification = validation.certification_id) ON educators3.id_educator = validation.commissioner1) ON educators2.id_educator = validation.commissioner2) ON educators.id_educator = validation.commissioner3) ";
              else
                $zapros = "select id_pk,concat(Surname,' ',Ename,' ',Patronomic) ,Doc_num ,Doc_date ,num_hours ,concat(title_kind) ,concat(title_institution) ,Program ,professional_module ,concat(title_specialty,' ',name_specialty) ,concat(title_direction)
from (post inner join (post as post2 inner join (training_direction inner join (specialty inner join (institution inner join (kind_training inner join (educators inner join pk
on educators.id_educator=pk.educator_id) on kind_training.id_kind_training=pk.kind_tr_id) on institution.id_institution=pk.institution_id) on specialty.id_specialty=pk.specialty_id) on training_direction.id_training_direction=pk.training_direction_id)on post2.id_post=educators.post_id1) on post.id_post=educators.post_id2)
";
            break;
            case 'validation':
                $zapros = "SELECT id_validation,CONCAT( educators4.Surname,' ', educators4.Ename,' ',educators4.Patronomic), CONCAT( categoryname ) , CONCAT( certificationname ) , date_validation, num_order, CONCAT( educators3.Surname,  ' ', educators3.Ename,  ' ', educators3.Patronomic ) , CONCAT( educators2.Surname,  ' ', educators2.Ename,  ' ', educators2.Patronomic ) , CONCAT( educators.Surname,  ' ', educators.Ename,  ' ', educators.Patronomic ) FROM (educators INNER JOIN (educators AS educators2 INNER JOIN (educators AS educators3 INNER JOIN(certification INNER JOIN (category INNER JOIN (educators AS educators4 INNER JOIN validation ON educators4.id_educator = validation.educator_id) ON category.id_category = validation.category_id) ON certification.id_certification = validation.certification_id) ON educators3.id_educator = validation.commissioner1) ON educators2.id_educator = validation.commissioner2) ON educators.id_educator = validation.commissioner3)";
            break;

        }

        // SELECT id_validation, CONCAT( educators4.Surname,' ', educators4.Ename,' ',educators4.Patronomic) as educ, CONCAT( categoryname ) , CONCAT( certificationname ) , date_validation, num_order, CONCAT( educators3.Surname,  ' ', educators3.Ename,  ' ', educators3.Patronomic ) , CONCAT( educators2.Surname,  ' ', educators2.Ename,  ' ', educators2.Patronomic ) , CONCAT( educators.Surname,  ' ', educators.Ename,  ' ', educators.Patronomic ) FROM (educators INNER JOIN (educators AS educators2 INNER JOIN (educators AS educators3 INNER JOIN(certification INNER JOIN (category INNER JOIN (educators AS educators4 INNER JOIN validation ON educators4.id_educator = validation.educator_id) ON category.id_category = validation.category_id) ON certification.id_certification = validation.certification_id) ON educators3.id_educator = validation.commissioner1) ON educators2.id_educator = validation.commissioner2) ON educators.id_educator = validation.commissioner3)


        // echo $zapros.$where;
        $dbresult = mysqli_query($db, $zapros . $where);
        // $err[]=$zapros.$where;
        if (!$dbresult) die("Ошибка доступа к таблице");

        while ($training = $dbresult->fetch_array())
        {
            $arr['data'][] = $training;
        }
        if (isset($arr['data'])) return $arr;
        else return "Не найдено!";
    }
}

?>
