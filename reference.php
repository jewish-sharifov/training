<?
function text($age) {
   //var $txt;
    $count = $age % 100;
    if ($count >= 5 && $count <= 20) {
        $txt ='лет';
    } else {
        $count = $count % 10;
        if ($count == 1) {
           $txt = 'год';
        } else if ($count >= 2 && $count <= 4) {
            $txt = 'года';
        } else {
            $txt = 'лет';
        }
    }
    return $txt;


}?>
<?
require('connect.php');
$id=$_GET['id'];
$res=mysqli_query($db,"
SELECT l.*, ed.*, a.*, k.*, v.*, e.*, o.*, p.*, ad.*, c.*, ca.*, t.*, s.*,i.*, pk.*
FROM
educators e
LEFT JOIN office o ON id_office = e.office_id
LEFT JOIN validation v ON v.educator_id=e.id_educator
LEFT JOIN pk ON pk.educator_id=e.id_educator
LEFT JOIN kind_training k ON k.id_kind_training=pk.kind_tr_id
LEFT JOIN academic_title a ON a.id_academictitle=e.academictitle_id
LEFT JOIN education ed ON ed.educator_id=e.id_educator
LEFT JOIN level_education l ON l.id_level=ed.level_id
LEFT JOIN institution i ON i.id_institution = pk.institution_id
LEFT JOIN training_direction t ON t.id_training_direction = pk.training_direction_id
LEFT JOIN category ca ON ca.id_category = v.category_id
LEFT JOIN certification c ON c.id_certification = v.certification_id
LEFT JOIN academic_degree ad ON ad.id_academic = e.academic_id
LEFT JOIN post p ON p.id_post = e.post_id1 OR p.id_post = e.post_id2
LEFT JOIN specialty s ON s.id_specialty = pk.specialty_id

WHERE e.id_educator='{$id}' AND pk.Doc_date > DATE_SUB(CURRENT_DATE(), INTERVAL 5 YEAR)
ORDER BY pk.institution_id DESC, pk.Doc_date DESC, ed.year_end"
);


$ch=false;
$c_st=1;
$c_pk=1;

$data = [];
$i=0;
while($row=$res->fetch_assoc()){

        if($i == 0) $data = $row;

        if(!isset($data['cert'][$row['num_order']]))
                $data['cert'][$row['num_order']] = [
                        'certificationname'=>$row['certificationname'],
                        'date_validation'=>$row['date_validation'],
                        'categoryname'=>$row['categoryname'],
                        'num_order'=>$row['num_order'],
                ];


        if(!isset($data['kv'][$row['Program']]))
                $data['kv'][$row['Program']]=[
                        'title_kind'=>$row['title_kind'],
                        'title_institution'=>$row['title_institution'],
                        'Program'=>$row['Program'],
                        'Doc_date'=>$row['Doc_date'],
                        'num_order'=>$row['num_order'],
                        'title_direction'=>$row['title_direction'],
                ];

        if(!isset($data['educ'][$row['year_end']]))
                $data['educ'][$row['year_end']]=[
                        'levelname'=>$row['levelname'],
                        'educ_organization'=>$row['educ_organization'],
                        'educ_specialty'=>$row['educ_specialty'],
                ];


        /*if($ch==false){
                $fio=$row['fio'];
                $date_validation=$row['date_validation'];
                $num_order=$row['num_order'];
                $academictitle=$row['academictitle'];
                $levelname=$row['levelname'];
                $age=$row['age'];
                $ch=true;
        }
        if($row['kind_tr_id']==1){//pk
                $pk[]=array(
                        'Doc_num'=>$row['Doc_num'],
                        'Doc_date'=>$row['Doc_date'],
                );
                $c_pk++;
        }
        if($row['kind_tr_id']==2){//st
                $st[]=array(
                        'Doc_num'=>$row['Doc_num'],
                        'Doc_date'=>$row['Doc_date'],
                );
                $c_st++;
        }*/
        $i++;
}

if(0==1 && $c_pk==1 and $c_st==1)
        echo "Недостаточно данных для формирования справки отсутствую данные по <b>повышению квалификации</b> и по <b>стажировкам</b>";
else {
        $pedaqoq = date('Y')- date('Y', strtotime($data['date_start_employment']));
        $employ = date('Y')- date('Y', strtotime($data['date_start_education_activ']));
        $dataob=date('Y')- date('Y', strtotime($data['date_start_employment']));


?>

<div>
        <div id=spr><center><b>Справка</b></center></div>
        <table border="1" align="center" width="100%" cellpadding="10" cellspacing="0">
                <tr>
                        <td width="50%">ФИО: <?=$data['Surname'].' '.$data['Ename'].' '.$data['Patronomic']?></td>
                        <td width="25%">Дата рождения: <?=$data['birthday']?></td>
                        <td width="25%">Ученая степень: <?=$data['academictitle']?></td>
                </tr>
                <tr>
                        <td width="250">Общий стаж работы </td>
                        <td colspan="2"><?=$dataob?> <?=(text($dataob))?></td>
                </tr>
                <tr>
                        <td width="250">Педагогический стаж работы</td>
                        <td colspan="2"><?=$pedaqoq?> <?=(text($pedaqoq))?></td>
                </tr>
                <tr>
                        <td width="250">Стаж в учебном заведение</td>
                        <td colspan="2"><?=$employ?> <?=(text($employ))?></td>
                </tr>
        </table>
        <div id=spr><center>Образование преподователя</center></div>
        <table border="1" align="center" width="100%" cellpadding="10" cellspacing="0">
                <?if(count($data['educ']) > 0):foreach($data['educ'] as $educ):?>
                        <tr>
                                <td width="250">Уровень образования</td>
                                <td colspan="2"><?=$educ['levelname']?></td>
                        </tr>
                        <tr>
                                <td width="250">Образовательная организация</td>
                                <td colspan="2"><?=$educ['educ_organization']?></td>
                        </tr>
                        <tr>
                                <td width="250">Специальность преподователя</td>
                                <td colspan="2"><?=$educ['educ_specialty']?></td>
                        </tr>


                        <tr>
                                <td width="250">Дата окончания</td>
                                <td colspan="2"><?=$educ['date_start_employment']?></td>
                        </tr>


                        <tr><td colspan="2">&nbsp;</td></tr>
                <?endforeach; endif?>
        </table>
        <div id=spr><center>Повышение квалификации</center></div>
        <table border="1" align="center" width="100%" cellpadding="10" cellspacing="0">
                <tr>
                        <td align="center">Форма <br/> прохождения</td>
                        <td align="center">Место прохождения</td>
                        <td align="center">Наименование <br/> программы</td>
                        <td align="center">Срок <br/> прохождения</td>
                        <td align="center">№ приказа</td>
                        <td align="center">Направление <br/> подготовки</td>
                </tr>
                <?if(count($data['kv']) > 0):foreach($data['kv'] as $kv):?>
                        <tr>
                                <td align="center"><?=$kv['title_kind']?></td>
                                <td align="center"><?=$kv['title_institution']?></td>
                                <td align="center"><?=$kv['Program']?></td>
                                <td align="center"><?=$kv['Doc_date']?></td>
                                <td align="center"><?=$kv['num_order']?></td>
                                <td align="center"><?=$kv['title_direction']?></td>
                        </tr>
                <?endforeach; endif?>
        </table>
        <div id=spr><center>Аттестация</center></div>
        <table border="1" align="center" width="100%" cellpadding="10" cellspacing="0">
                <tr>
                        <td align="center">Вид <br/> аттестации</td>
                        <td align="center">Дата аттестации</td>
                        <td align="center">        Категория        </td>
                        <td align="center">Номер приказа</td>
                </tr>
                <?if(count($data['cert']) > 0):foreach($data['cert'] as $cert):?>
                        <tr>
                                <td align="center"><?=$cert['certificationname']?></td>
                                <td align="center"><?=$cert['date_validation']?></td>
                                <td align="center"><?=$cert['categoryname']?></td>
                                <td align="center"><?=$cert['num_order']?></td>
                        </tr>
                <?endforeach; endif?>
        </table>
</div>
<?}?>
