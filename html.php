<?

class HTML{
        public $auth;
        public $n;
        public $p;
        public $l;
        public $tw=array('pkpp','pkst','validation','pk');
        function __construct(){
                if(isset($_SESSION['is_auth'])){
                $this->auth=true;
                $this->n=$_SESSION["name"];
                $this->p=$_SESSION["patronomic"];
                $this->l=$_SESSION['login'];
                }
        }
        function order(){
                Global $request, $select_categories, $select_certification;

                if($request['table_name'] == 'educators')
                        $params = ['Surname', 'должностям'];
                else
                        $params = ['educ', 'дате документа'];
                $a='<form method="get" action="">
                                <input type="hidden" name="act" value="show_table"/>
                                <input type="hidden" name="table_name" value="'.$request['table_name'].'"/>
                                <input type="radio" name="order_field" '.($request['order_field'] == $params[0]?'checked':null).' value="'.$params[0].'"> по фамилии | <input type="radio" name="order_field" '.($request['order_field'] == 'Doc_date'?'checked':null).' value="Doc_date"> по '.$params[1].'
                                <select name="order">
                                    <option value="">по возрастанию</option>
                                    <option '.($request['order'] =='desc'?'selected':null).' value="desc">по убыванию</option>
                                </select>
                                '.($request['table_name'] == 'validation'?
                                '<select name="category" class="filter">
                                    <option value="all">По умолчанию</option>
                                    '.$select_categories.'
                                </select>
                                <select name="view" class="filter">
                                    <option value="all">По умолчанию</option>
                                    '.$select_certification.'
                                </select>':null).'
                                <input type="submit" name="submit" value="Сортировать"></form>';
                return $a;
        }
        function errors($err){
        $a='<div class="errors">';
                // print_r($err);
                foreach($err as $key=>$val){
                        if(is_array($val))
                foreach($val as $k=>$v){
                $a.='<span>'.$k.':'.$v.'</span><br>';
                }
                else $a.='<span>'.$key.':'.$val.'</span><br>';
                        }
                        $a.='</div>';
                return $a;
        }
        function head($t=false){
                return '<html><head>'
                        .'<title>'.$t.'</title>'
                        .'<link rel="stylesheet" type="text/css" href="styles/style.css">'
                        .'<link rel="stylesheet" type="text/css" href="styles/load.css">'
                        .'<script>
                        table_name="'.req('table_name').'";
                        category="'.req('category').'";
                        search="'.req('search').'";
                        </script>'
                        .'<script src="/js/jquery-3.1.1.min.js"></script>'
                        .'<script src="/js/scripts.js"></script>'
                        .'</head><body>';
        }
        function title(){
                if(req('table_name'))
                        $n=req('table_name');
                elseif(req('search')!='Not_usual')
                        $n=req('search');
                elseif (req('on_field')){
                        $n=req('on_field');
                }else
                        $n='index';
                if($this->auth)
        switch ($n){
        case 'educators': $e = "Сотрудники";        break;
        case 'validation':        $e =  "Аттестация";        break;
        case 'pkpp': $e =  "Повышение квалификации"; break;
        case 'pkst':        $e = "Повышение квалификации в форме стажировки"; break;
        case 'education':        $e = "Образование преподавателей";        break;
        case 'level_education':        $e = "Уровни образования";        break;
        case 'category': $e = "Категории";        break;
        case 'certification':        $e = "Вид аттестации";        break;
        case 'post':        $e = "Должность";        break;
        case 'academic_degree':        $e = "Ученая степень";        break;
        case 'academic_title':        $e = "Ученое звание";        break;
        case 'office':        $e = "Отделение";        break;
        case 'pk_pck':        $e = "ПК/ПЦК";        break;
        case 'institution':        $e = "Учреждения";        break;
        case 'specialty':        $e = "Специальности";        break;
        case 'training_direction':        $e = "Направление подготовки";        break;
        case 'kind_training':        $e = "Форма обучения";        break;
        case 'users':        $e = "Пользователи";        break;
        case 'index':        $e = "Главная";        break;
        case 'on_Surname':        $e = "Поиск по фамилии сотрудника";        break;
        case 'on_atsurname':        $e = "Поиск по должности аттестации";        break;
        case 'on_atspec':        $e = "Поиск по специальности аттестации";        break;
        case 'on_atoffice':        $e = "Поиск по отделению аттестации";        break;
        case 'on_post':        $e = "Поиск по должности сотрудника";        break;
        case 'on_specialty':        $e = "Поиск по специальности";        break;
        case 'on_date_st':        $e = "Поиск по дате стажировок";        break;
        case 'srok_st':        $e = "Поиск просроченных данных по стажировкам";        break;
        case 'srok_pk':        $e = "Поиск просроченных данных по повышению квалификации";        break;
        case 'on_date_pk':        $e = "Поиск по дате повышения квалификации";        break;
        case 'on_kind_training':        $e = "Поиск по форме обучения";        break;
        case 'on_null_doc':        $e = "Поиск пустых документов по повышению квалификации и стажировок";break;
        case 'on_validation':        $e = "Поиск по просроченным атестациям";break;
        default: $e='';
        } else $e = "Авторизуйтесь";
        return $e;
}
        function footer(){
                return $this->preloader()
                        .'<div id="okno"><div id="fff"></div></div>'
                        .'</body></html>';
        }
        function container($tit,$c,$tab){
                return '<div class="bg"></div>'
                        .'<div id="container">'
                        .'<div class="title"><h2 id="table_title" name="title">'.$tit.'</h2></div>'
                        .'<div id="content">'.$c.'</div>'
                        .$tab
                        .'</div>';
        }
        function header($t=false){
                return $this->head($t).'<header>'.$this->header_text().$this->inout().$this->menu().'</header>';
        }

        function inout(){
                if ($this->auth)
                        return '<div class=open>'
                                .'<form method="POST" action="index.php">'
                                .'<span>'.$this->n.' '.$this->p.'</span>'
                                .'<input class="n1" type=submit name="exit" value="Выход"/>'
                                .'</form></div>';
                else
                        return '<div class=open>Вход</div>'
                                .'<div id=forma>'

                                .'<form class="authoriz" method="POST" action="index.php">'
                                .'<br><label for="a" class="a"> Логин</label> <br>'
                                .'<input class="lp" type="text" placeholder="" name="login" required><br><br>'
                                .'<label for="a" class="a"> Пароль</label> <br>'
                                .'<input  class="lp" type="password" placeholder="" name="password" required><br><br>'
                                .'<input class="buttons"  type="submit" value="Войти" name="logon">'
                                .'<input class="cancel"  type="button" value="Скрыть"><br><br>'
                                .'</form>'
                                .'</div>';
        }
        function header_text(){
                return '<div id=logo></div>'
                                .'<div id=mgtu>ФГБОУ ВО "Магнитогорский государственный технический университет им. Г.И. Носова"'
                                .'<div id=mgtu>Многопрофильный колледж</div>';

        }
        function getMenu($parent_id=0, $qmenu=''){
          global $db;

          if(!$parent_id) $html = '<div id=menu><ul class="dropdown">';

          if(empty($qmenu))
            $qmenu = mysqli_query($db,"SELECT id, href, name, not_access FROM menu WHERE parent_id = '{$parent_id}'");

          while($r = mysqli_fetch_object($qmenu)){
            $qchild = mysqli_query($db,"SELECT id, href, name, not_access FROM menu WHERE parent_id = '{$r->id}'");

            if(!mysqli_num_rows($qchild))
                $li='<li><a href="'.$r->href.'">'.$r->name.'</a></li>';
            else
                $li='<li class="dropdown-top"><a class="dropdown-top">'.$r->name.'</a><ul class="dropdown-inside">'.$this->getMenu($r->id, $qchild).'</ul></li>';

            if($this->l=='admin' || strpos(rtrim($r->not_access,',').',', $this->l.',') ===false)
                $html.=$li;

          }

          if(!$parent_id) $html.='</ul></div>';

          return $html;
        }
        function menu(){
              if($this->auth) return $this->getMenu();
        }
        function apw($s=''){
                Global $request;
                $a='';
                if($s!='')
                        $s='='.$s;
                $t=req('table_name');
                if ($this->auth){
                $a='<div id=func><ul>';
                                if(req('table_name') or req('search'))$a.='<li class=add title="Добавить запись" onclick="XHR(\'add.php?'.http_build_query($request).'&surname'.$s.'\', fff);"></li>'
                                .'<li class=print title="Распечатать" onclick="javascript:CallPrint(\'container\');"></li>';
                                if (in_array($t,$this->tw) or req('search') and req('on_field')) $a.='<li class="word" data-table-name="'.$t.'" data-search="'.req('search').'" data-query="'.http_build_query($request).'" title="Импорт в Word" ></li>';
                                $a.='</ul></div>';
                }
                return $this->l=='admin'?$a:'';
        }
        function preloader(){
                return '<div id=load><div class="cssload-triangles">'
                                        .'<div class="cssload-tri cssload-invert"></div>'
                                        .'<div class="cssload-tri cssload-invert"></div>'
                                        .'<div class="cssload-tri"></div>'
                                        .'<div class="cssload-tri cssload-invert"></div>'
                                        .'<div class="cssload-tri cssload-invert"></div>'
                                        .'<div class="cssload-tri"></div>'
                                        .'<div class="cssload-tri cssload-invert"></div>'
                                        .'<div class="cssload-tri"></div>'
                                        .'<div class="cssload-tri cssload-invert"></div>'
                                .'</div></div>';
        }
        function search_form(){
                Global $db,$request;
                        $a='';
                switch(req('search')){
                        case 'on_Surname':
                        if(in_array($_SESSION['login'], ['st1']))
                            $_sql = "SELECT educators4.Surname name FROM (educators INNER JOIN (educators AS educators2 INNER JOIN (educators AS educators3 INNER JOIN (certification INNER JOIN (category INNER JOIN (post INNER JOIN (educators AS educators4 INNER JOIN validation ON educators4.id_educator = validation.educator_id ) ON post.id_post = educators4.post_id1 ) ON category.id_category = validation.category_id ) ON certification.id_certification = validation.certification_id ) ON educators3.id_educator = validation.commissioner1 ) ON educators2.id_educator = validation.commissioner2 ) ON educators.id_educator = validation.commissioner3 ) GROUP BY name ORDER BY name";
                        else
                            $_sql = "Select Surname as name From educators group by Surname";
                        $dbresult = mysqli_query($db,$_sql);
                        while($row = $dbresult->fetch_assoc()){$a.='<option>'.$row['name'].'</option>';}
                                return '<form id="search_form"  method="post" action="index.php?on_field=surname&in=pk&'.http_build_query($request).'">
                                <input type="search" autocomplete="off" name="zapros" list="surn" placeholder="Поиск">
                                <input  type="submit" value="Найти" >
                                </form>
                                <datalist id="surn">'.$a.'</datalist>';
                        break;
                        case 'on_atsurname':
                        $dbresult = mysqli_query($db,"SELECT post.title_job pp FROM (educators INNER JOIN (educators AS educators2 INNER JOIN (educators AS educators3 INNER JOIN (certification INNER JOIN (category INNER JOIN (post INNER JOIN (educators AS educators4 INNER JOIN validation ON educators4.id_educator = validation.educator_id ) ON post.id_post = educators4.post_id1 ) ON category.id_category = validation.category_id ) ON certification.id_certification = validation.certification_id ) ON educators3.id_educator = validation.commissioner1 ) ON educators2.id_educator = validation.commissioner2 ) ON educators.id_educator = validation.commissioner3 ) GROUP BY pp ORDER BY pp");
                        while($row = $dbresult->fetch_assoc()){$a.='<option>'.$row['pp'].'</option>';}
                                return '<form id="search_form"  method="post" action="index.php?on_field=title_job&'.http_build_query($request).'">
                                <input type="search" autocomplete="off" name="zapros" list="surn" placeholder="Поиск">
                                <input  type="submit" value="Найти" >
                                </form>
                                <datalist id="surn">'.$a.'</datalist>';
                        break;
                        case 'on_atspec':
                        $dbresult = mysqli_query($db,"SELECT specialty.name_specialty spec FROM ( educators INNER JOIN (educators AS educators2 INNER JOIN (educators AS educators3 INNER JOIN (certification INNER JOIN (category INNER JOIN (specialty INNER JOIN (pk INNER JOIN (educators AS educators4 INNER JOIN validation ON educators4.id_educator = validation.educator_id) ON pk.educator_id = educators4.id_educator) ON pk.specialty_id = specialty.id_specialty) ON category.id_category = validation.category_id) ON certification.id_certification = validation.certification_id) ON educators3.id_educator = validation.commissioner1) ON educators2.id_educator = validation.commissioner2) ON educators.id_educator = validation.commissioner3) GROUP BY spec ORDER BY spec");
                        while($row = $dbresult->fetch_assoc()){$a.='<option>'.$row['spec'].'</option>';}
                                return '<form id="search_form"  method="post" action="index.php?on_field=name_specialty&'.http_build_query($request).'">
                                <input type="search" autocomplete="off" name="zapros" list="surn" placeholder="Поиск">
                                <input  type="submit" value="Найти" >
                                </form>
                                <datalist id="surn">'.$a.'</datalist>';
                        break;
                        case 'on_atoffice':
                        $dbresult = mysqli_query($db,"SELECT office.officename off FROM (educators INNER JOIN (educators AS educators2 INNER JOIN (educators AS educators3 INNER JOIN (certification INNER JOIN (category INNER JOIN (office INNER JOIN (educators AS educators4 INNER JOIN validation ON educators4.id_educator = validation.educator_id )ON office.id_office = educators4.office_id )ON category.id_category = validation.category_id )ON certification.id_certification = validation.certification_id )ON educators3.id_educator = validation.commissioner1 )ON educators2.id_educator = validation.commissioner2 )ON educators.id_educator = validation.commissioner3 ) GROUP BY off ORDER BY office.id_office");
                        while($row = $dbresult->fetch_assoc()){$a.='<option>'.$row['off'].'</option>';}
                                return '<form id="search_form"  method="post" action="index.php?on_field=officename&'.http_build_query($request).'">
                                <input type="search" autocomplete="off" name="zapros" list="surn" placeholder="Поиск">
                                <input  type="submit" value="Найти" >
                                </form>
                                <datalist id="surn">'.$a.'</datalist>';
                        break;
                        case 'on_post':
                                $dbresult = mysqli_query($db,"Select title_job as name From post");
                                while($row = $dbresult->fetch_assoc()){$a.='<option>'.$row['name'].'</option>';}
                                return '<form id="search_form"  method="post" action="index.php?on_field=title_job&'.http_build_query($request).'">
                                <input type="search" name="zapros" list="job" placeholder="Поиск">
                                <input type="submit" value="Найти" >
                                </form>
                                <datalist id="job">'.$a.'</datalist>';
                        break;
                        case 'on_specialty':
                                $dbresult = mysqli_query($db,"Select title_specialty as name, name_specialty as descrip From specialty");
                                while($row = $dbresult->fetch_assoc()){$a.='<option value="'.$row['name'].'">'.$row['descrip'].'</option>';}
                                return '<form id="search_form"  method="post" action="index.php?on_field=title_specialty&'.http_build_query($request).'">
                                <input type="search" name="zapros" list="specialty" placeholder="Поиск">
                                <input type="submit" value="Найти" >
                                </form>
                                <datalist id="specialty">'.$a.'</datalist>';
                        break;
                        case 'on_date_st':
                                return '<form id="search_form"  method="post" action="index.php?on_field=date_st&'.http_build_query($request).'">
                                <input  type="date" name="zapros"  placeholder="Поиск">
                                <input type="submit" value="Найти" >
                                </form>';
                        break;
                        case 'on_date_pk':
                                return '<form id="search_form"  method="post" action="index.php?on_field=date_pk&'.http_build_query($request).'">
                                <input  type="date" name="zapros" >
                                <input type="submit" value="Найти" >
                                </form>';
                        break;
                        case 'on_kind_training':
                                return '<form id="search_form"  method="post" action="index.php?on_field=id_kind_tr&'.http_build_query($request).'">
                                <select  name="zapros">
                                <option  value="1">Повышение квалификации</option>
                                <option  value="2">Повышение квалификации в форме стажировки</option>
                                <option  value="3">Профессиональная переподготовка</option>
                                </select>
                                <input type="submit" value="Найти" >
                                </form>';
                        break;
                        case 'on_validation':
                                $a.='<form id="search_form"  method="post" action="index.php?on_field=date_validation&'.http_build_query($request).'">
                                с <input  type="date" name="zapros"> период
                                <select  name="period">
                                <option value="30">1 месяц</option>
                                <option value="60">2 месяца</option>
                                <option value="365">1 год</option>
                                </select>
                                вид аттестации <select  name="certification">
                                <option value="all">по всем</option>';
                                $select=mysqli_query($db,"select  id_certification,certificationname  from certification");
                                while($sel = $select->fetch_assoc()){$a.='<option value="'.$sel['id_certification'].'">'.$sel['certificationname'].'</option>';}
                                $a.='</select>        категория <select  name="category"><option value="all">по всем</option>';
                                $select=mysqli_query($db,"select id_category,categoryname  from category");
                                while($sel = $select->fetch_assoc()){$a.='<option value="'.$sel['id_category'].'">'.$sel['categoryname'].'</option>';}
                                $a.='</select>
                                <input type="submit" value="Найти" >
                                </form>';
                                return $a;
                        break;

                        case 'Not_usual':
                        break;
                        default:
                        return 'Ошибка формирования формы';


                }
        }
}

?>
